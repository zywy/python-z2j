#Paper, Rock, Sissors - game

import random
print("Welcome, please sit and let's play")
max_games= int(input("Enter numbers of games: "))
print("To win, you need to show stronger signs, can you make it?")
player1_score=0
computer_score=0
options=("rock","scissors","paper")

for x in range(max_games):
    user_choice= input("Enter your sign: ").lower()
    comp_choice= random.choice(options)
    if user_choice==comp_choice:
        print("It's a draw!")
    elif user_choice== "rock" and comp_choice== "scissors":
        print("Player wins the round")
        player1_score= player1_score + 1
    elif user_choice== "rock" and comp_choice== "paper":
        print("Player loses the round")
        computer_score= computer_score + 1
    elif user_choice== "paper" and comp_choice== "rock":
        print("Player wins the round")
        player1_score= player1_score + 1
    elif user_choice== "paper" and comp_choice== "scissors":
        print("Player loses the round")
        computer_score= computer_score + 1
    elif user_choice== "scissors" and comp_choice== "paper":
        print("Player wins the round")
        player1_score= player1_score + 1
    elif user_choice== "scissors" and comp_choice== "rock":
        print("Player loses the round")
        computer_score= computer_score + 1
if  player1_score== computer_score:
    print("Its a draw")
elif player1_score > computer_score:
    print("Player wins the game")
elif computer_score > player1_score:
    print("Computer wins the game")
