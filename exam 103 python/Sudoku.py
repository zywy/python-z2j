import random

def generate_square():
    numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
    square =  random.sample(numbers, 9)
    return square

def generate_board():
    board = []
    for i in range(3):
        for j in range(3):
            square = generate_square()
            board += square
    return board

def print_board(board):
    for i in range(9):
        if i % 3 == 0 and i != 0:
            print("- " * 11)
        for j in range(9):
            if j % 3 == 0 and j != 0:
                print("|", end=" ")
            print(board[i * 9 + j], end=" ")
        print()

board = generate_board()
print_board(board)
