def sort(x):
    n = len(x)
    for i in range(n - 1): 
        for j in range(n - 1 - i):
            if x[j] > x[j + 1]:
                x[j], x[j + 1] = x[j + 1], x[j]


nums = [[],[1,2,5,3,1,7,9,1,12,83,1,5,3,2],[1,1,1,1,1,1,1,2,1,1,1],[2,2,2,2],[1,2],[2,1]]

for group in nums:
    sort(group)
    
for group in nums:
    print(group)
