import pygame
import math
import random

pygame.init()
WIDTH = 800
HEIGHT = 550
win = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("CRAZY RABBITS")
clock = pygame.time.Clock()
font = pygame.font.Font('assets/fonts/Font.ttf', 32)

enemies_images = [[], [], []]
targets = {
    1: [3, 0, 0],
    2: [5, 1, 0],
    3: [6, 2, 0],
    4: [7, 4, 1],
    5: [6, 6, 2],
    6: [8, 6, 3],
    7: [10, 7, 4],
    8: [10, 9, 6],
    9: [10, 10, 8],
    10: [10, 10, 10],
}

lives = 3

bgs = []
for i in range(1, 11):
    bgs.append(pygame.image.load(f'assets/bgs/{i}.jpg'))

start_button_width = 500
start_button_height = 500

play_button_image = pygame.image.load('assets/menu/play again.png')
play_button_image = pygame.transform.scale(play_button_image, (start_button_width, start_button_height))
play_button_rect = play_button_image.get_rect()
play_button_rect.center = (WIDTH // 2, HEIGHT - play_button_rect.height // 2 - 10)

for i in range(1, 4):
    image = pygame.image.load(f'assets/rabbits/rabbit{i}.png')
    scaled_image = pygame.transform.scale(image, (int(image.get_width() * 0.30), int(image.get_height() * 0.30)))
    enemies_images[i - 1].append(scaled_image)

level = 1
max_rabbits = 3
rabbit_timer = 5000 - (level - 1) * 500
game_over = False
missed_rabbit_count = 0
pudlo_sound = pygame.mixer.Sound('assets/sounds/pudlo.mp3')

show_rabbit = False
show_timer = 0
rabbit_show_duration = [5000 - (level - 1) * 500 for level in range(1, 11)]
last_rabbit_time = pygame.time.get_ticks()
rabbits_clicked = 0
rabbits_to_click = 10
current_rabbits = []

def check_hit(rabbit):
    mouse_pos = pygame.mouse.get_pos()
    rabbit_x, rabbit_y, rabbit_image, _ = rabbit
    if rabbit_x <= mouse_pos[0] <= rabbit_x + rabbit_image.get_width() and \
            rabbit_y <= mouse_pos[1] <= rabbit_y + rabbit_image.get_height():
        return True
    return False

for i in range(1, 4):
    for j in range(len(enemies_images[i - 1])):
        enemies_images[i - 1][j] = pygame.transform.scale(enemies_images[i - 1][j], (int(enemies_images[i - 1][j].get_width() * 0.25), int(enemies_images[i - 1][j].get_height() * 0.25)))

last_rabbit_time = 0

run = True

while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

        if event.type == pygame.MOUSEBUTTONDOWN:
            if game_over:
                if play_button_rect.collidepoint(event.pos):
                    level = 1
                    lives = 3
                    spawn_timer = 0
                    max_rabbits = 3
                    rabbit_timer = 10000 - (level - 1) * 1000
                    game_over = False
                    missed_rabbit_count = 0
            else:
                hit = False
                if show_rabbit:
                    for rabbit in current_rabbits:
                        if check_hit(rabbit):
                            hit = True
                            current_rabbits.remove(rabbit)
                            show_rabbit = False
                            print("Trafiony!")
                            rabbits_clicked += 1
                            if rabbits_clicked >= rabbits_to_click:
                                rabbits_clicked = 0
                                level += 1
                                max_rabbits = targets[level][0]
                                rabbit_timer = 5000 - (level - 1) * 500
                                current_rabbits.clear()
                            break
                    if not hit:
                        print("Pudło!")
                        missed_rabbit_count += 1
                        pudlo_sound.play()

                        if missed_rabbit_count >= 1:
                            lives -= 1
                            missed_rabbit_count = 0

    win.blit(bgs[level - 1], (0, 0))

    if not show_rabbit and random.random() < show_timer:
        show_timer = 0
        if len(current_rabbits) < max_rabbits:
            rabbit_index = random.randint(0, len(targets[level]) - 1)
            rabbit_width = int(enemies_images[rabbit_index][0].get_width() * 0.25)
            rabbit_height = int(enemies_images[rabbit_index][0].get_height() * 0.25)
            rabbit_x = random.randint(50, WIDTH - rabbit_width - 50)
            rabbit_y = random.randint(50, HEIGHT - rabbit_height - 50)
            rabbit_image = enemies_images[rabbit_index][0]
            rabbit_data = [rabbit_x, rabbit_y, rabbit_image, rabbit_index]
            current_rabbits.append(rabbit_data)
            show_rabbit = True
            show_timer = pygame.time.get_ticks()
            last_rabbit_time = pygame.time.get_ticks()
    else:
        show_timer += 0.01

    current_time = pygame.time.get_ticks()
    if  show_rabbit and current_time - last_rabbit_time >= rabbit_show_duration[level - 1]:
        show_rabbit = False
        missed_rabbit_count += 1
        pudlo_sound.play()
        
        if missed_rabbit_count >= 1:
            lives -= 1
            missed_rabbit_count = 0
            
        last_rabbit_time = current_time
        
    if not show_rabbit and random.random() < show_timer:
        show_timer = 0
        if len(current_rabbits) < max_rabbits:
            current_rabbits.clear()
            if len(current_rabbits) == 3:
                continue
                rabbit_index = random.randint(0, len(targets[level]) - 1)
                rabbit_width = int(enemies_images[rabbit_index][0].get_width() * 0.25)
                rabbit_height = int(enemies_images[rabbit_index][0].get_height() * 0.25)
                rabbit_x = random.randint(50, WIDTH - rabbit_width - 50)
                rabbit_y = random.randint(50, HEIGHT - rabbit_height - 50)
                rabbit_image = enemies_images[rabbit_index][0]
                rabbit_data = [rabbit_x, rabbit_y, rabbit_image, rabbit_index]
                current_rabbits.append(rabbit_data)
                show_rabbit = True
                last_rabbit_time = current_time
        else:
            show_timer += 0.01
        
    for rabbit in current_rabbits:
        rabbit_x, rabbit_y, rabbit_image, _ = rabbit
        win.blit(rabbit_image, (rabbit_x, rabbit_y))

    if lives <= 0:
        game_over = True

    if game_over:
        win.blit(play_button_image, play_button_rect)

    lives_text = font.render(f"Lives: {lives}", True, (255, 255, 255))
    win.blit(lives_text, (WIDTH - lives_text.get_width() - 10, 10))

    level_text = font.render(f"Level: {level}", True, (255, 255, 255))
    win.blit(level_text, (WIDTH - level_text.get_width() - 680, 10))

    pygame.display.flip()
    clock.tick(60)

pygame.quit()
