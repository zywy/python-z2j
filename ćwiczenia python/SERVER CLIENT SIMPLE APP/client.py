import socket
import json
import sys

class Client:
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect(("127.0.0.1", 61247))

    def send_message(self, message):
        self.s.sendall(message.encode("utf-8"))
        data = self.s.recv(1024)
        try:
            response = json.loads(data.decode("utf-8"))
        except json.JSONDecodeError as e:
            print("Server response:", data.decode("utf-8"))
            print("JSON Decode Error:", e)
            return

        if "help" in response:
            print(response["help"])
        elif "uptime" in response:
            print(response["uptime"])
        elif "info" in response:
            print(response["info"])
        elif "stop" in response:
            print(response["stop"])
            self.s.close()
            sys.exit(0)
        else:
            print("Unknown response:", response)


if __name__ == "__main__":
    client = Client()

    while True:
        command = input("Enter command (help, uptime, info, stop): ")
        client.send_message(command.lower())

        if command.lower() == "stop":
            break
