from server import Server
from options import Options
class MAIN:
    HOST = "127.0.0.1"
    PORT = 61247
    INFO = "version: 0.0.1. ALPHA, from 24.07.2023"

if __name__ == "__main__":
    options_instance = Options()
    server = Server(MAIN.HOST, MAIN.PORT, MAIN.INFO, options_instance)
    server.start()