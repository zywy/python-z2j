import json
from datetime import datetime

def get_help():
    help_commands_list = [
        "uptime - server work time",
        "info - version of the server",
        "help - the list of available commands",
        "stop - stopping server and client"
    ]
    help_text = "\n".join(help_commands_list)
    return json.dumps({"help": help_text})

class Options:
    def __init__(self):
        self.start_time = datetime.now()

    def get_time(self):
        current_time = datetime.now()
        return str(current_time - self.start_time)

    def uptime(self):
        uptime = self.get_time()
        return json.dumps({"uptime": uptime})

    def info(self, server_info):
        return json.dumps({"info": server_info})

    def help(self):
        return get_help()

    def stop(self, conn):
        response = {"stop": "Server and client stopped."}
        conn.sendall(json.dumps(response).encode("utf-8"))
        conn.close()
        return None
