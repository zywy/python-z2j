import socket
import json

class Server:
    def __init__(self, host, port, server_info, options):
        self.host = host
        self.port = port
        self.server_info = server_info
        self.options = options

    def start(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.host, self.port))
            s.listen()
            print(f"Server listening on {self.host}:{self.port}")
            try:
                while True:
                    conn, addr = s.accept()
                    with conn:
                        print(f"Connected to {addr}")
                        while True:
                            data = conn.recv(1024).decode("utf-8")
                            if not data:
                                break
                            response = self.handle_command(data, conn)
                            if response is None:
                                print("disconnected")
                                break

                            conn.sendall(response.encode("utf-8"))
            except KeyboardInterrupt:
                print("Server interrupted. Stopping...")
                return

    def handle_command(self, command, conn):
        if command == "uptime":
            return self.options.uptime()
        elif command == "info":
            return self.options.info(self.server_info)
        elif command == "help":
            return self.options.help()
        elif command == "stop":
            return self.options.stop(conn)
        else:
            return "Unknown command"
