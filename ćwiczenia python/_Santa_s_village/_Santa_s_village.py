#Santa's village


class santa_village:
    def init(self,name, population):
        self.name= name
        self.population= population
            
    def add_population(self):
        population= population + 1

    def work_shift(self, workers):
        self.workers= workers

    def sick_worker(self,sick):
        self.sick= sick
        sick = sick + 1

    def worker_off(self,worker_off):
        self.worker_off = worker_off
        worker_off = worker_off + 1

    def cure_sick(self, cure):
        self.cure_sick= cure_sick
        sick = sick - 1

class buildings:
    def init(self,name, max_capacity, current_workers, adress):
        self.name= name
        self.max_capacity= max_capacity
        self.current_workers= current_workers
        self.adress= adress
    def info(self):
        return f"This is a {self.name}, maximum number of workers here is {self.max_capacity}, currently there is {self.current_workers} workers. This building is on {self.adress}"

class santa_house(buildings):
    def init(self, name, max_capacity, current_workers, adress, gifts):
        self.gifts= gifts
    def add_gift(self):
        gifts = gifts + 1

class workshop(buildings):
    def init(self, name, max_capacity, current_workers, adress, toys, sweets):
        self.toys= toys
        self.sweets=  sweets

    def add_toys(self):
        toys= toys + 1

    def add_sweets(self):
        sweets= sweets + 1

class stable(buildings):
    def init(self, name, max_capacity, current_workers, adress, animals):
        self.animals= animals
    def add_animal(self):
        animals= animals + 1
        
class people:
    def init(name, age, internship, role):
        self.age = age
        self.internship = internship
        self.role= role
    def info(self):
        return f" name: {self.name}, age: {self.age}, it's his {self.internship} year of working here, he is {self.role}"

class elf(people):
    def init(name, age, internship, role, spiky_ears):
        self.spiky_ears= spiky_ears

class dwarf(people):
    def init(name, age, internship, role, beard):
        self.beard= beard

class santa(people):
    def init(name, age, internship, role, red_uniform):
        self.red_uniform= red_uniform

class reindeer_rider(people):
    def init(name, age, internship, role, driving_license):
        self.driving_license= driving_license
        
        
    
  

    

        
        
