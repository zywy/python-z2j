import tkinter as tk
from PIL import ImageTk, Image
import random

def computer_choice():
    choices = ["rock", "paper", "scissors"]
    return random.choice(choices)

def check_winner(player_choice, computer_choice):
    if player_choice == computer_choice:
        return "Remis!"
    elif (
        (player_choice == "rock" and computer_choice == "scissors")
        or (player_choice == "paper" and computer_choice == "rock")
        or (player_choice == "scissors" and computer_choice == "paper")
    ):
        return "Wygrałeś!"
    else:
        return "Przegrałeś!"

def play(player_choice):
    computer = computer_choice()
    result = check_winner(player_choice, computer)

    player_label.config(text=f"Twój wybór: {player_choice}")
    computer_label.config(text=f"Wybor komputera: {computer}")
    result_label.config(text=result)

window = tk.Tk()
window.title("Kamień, papier, nożyce")

rock_img = ImageTk.PhotoImage(Image.open("images/rock.png").resize((100, 100)))
paper_img = ImageTk.PhotoImage(Image.open("images/paper.png").resize((100, 100)))
scissors_img = ImageTk.PhotoImage(Image.open("images/scissors.png").resize((100, 100)))

rock_button = tk.Button(window, image=rock_img, command=lambda: play("rock"))
rock_button.pack(side=tk.LEFT)

paper_button = tk.Button(window, image=paper_img, command=lambda: play("paper"))
paper_button.pack(side=tk.LEFT)

scissors_button = tk.Button(window, image=scissors_img, command=lambda: play("scissors"))
scissors_button.pack(side=tk.LEFT)

player_label = tk.Label(window, text="Twój wybór: ")
player_label.pack()
computer_label = tk.Label(window, text="Wybór komputera: ")
computer_label.pack()
result_label = tk.Label(window, text="")
result_label.pack()

window.mainloop()
